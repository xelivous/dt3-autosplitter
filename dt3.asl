// Currently this file is tailored towards the any% run since that's the only one that anybody runs
// There should be enough options to handle a few other types of runs as well
// There's likely a better way to handle a few things but for now this is fine.

state("DT3_v1.5.2.4") {
    int mapID: 0x4452FC; //internal gamemaker room mapping
    
    double gameProgress: 0x286AB4, 0x4, 0x4890; // arbitrary internal progress number to check for cutscenes/etc
    double programChips: 0x286AB4, 0x4, 0x48B8;
    double gamePercent: 0x286AB4, 0x4, 0x48E0; // what percent of the items are found/etc
    double gameCompleted: 0x286AB4, 0x4, 0x5498; // if the game is done v handy

    /// achievements
    string50 tokenRecognitions: 0x286AB4, 0x4, 0x54C8, 0x0;
    string50 tokenRecognitionsTwo: 0x286AB4, 0x4, 0x54F0, 0x0;

    /// Game state data
    double inCutscene: 0x286AB4, 0x4, 0x4CA0; // a lot of things aren't counted as a cutscene apparently
    double inGameOver: 0x286AB4, 0x4, 0x4C50; // if the game over screen is displayed
    double isPaused: 0x286AB4, 0x4, 0x6A50; // if the pause menu is up, or the game is just generally paused like in a ""cutscene""
    string100 currentBoss: 0x286AB4, 0x4, 0x4D98, 0x0; // the name of the boss if one exists
    double bossTrack: 0x286AB4, 0x4, 0x4DB8; // 0 = no boss, 1+ = fighting boss?
    double bBossGallery: 0x286AB4, 0x4, 0x5240; // 0 = not in boss gallery, 1 = in boss gallery
}

startup {
    settings.Add("split_on_every_boss_start", true, "Split whenever you start a boss fight");
    settings.SetToolTip("split_on_every_boss_start", "In case you want to separate doing normal chapter movement from doing a boss fight");
    settings.Add("split_on_every_boss", true, "Split after every boss");
    settings.SetToolTip("split_on_every_boss", "Internally checks if the currentBossName value changes from \"has something\" to \"doesn't have something\".\nI don't think anything else sets this value so it should be fine.");
    settings.Add("split_on_every_chapter", false, "Split after every chapter starts");
    settings.SetToolTip("split_on_every_chapter", "There isn't a good way to do detect this.\nIt uses a manually defined map of gameProgress values");
    settings.Add("split_on_every_program_chip", false, "Split whenever a Program Chip is obtained");
    settings.SetToolTip("split_on_every_program_chip", "I'm not sure if this is useful since it's always obtained after a boss.\nmaybe it can be used instead of splitting on bosses.");
    settings.Add("split_on_game_completed", false, "Split when the gameCompleted value changes to true");
    settings.SetToolTip("split_on_game_completed", "This gets set immediately before returning to the title after the epilogue.\nMay or may not be useful.");
    settings.Add("split_on_achievement", false, "Split every time an achievement is gotten");
    settings.SetToolTip("split_on_achievement", "Is probably useful if you're running an AllAchievements% category?");
    settings.Add("split_on_boss_gallery", false, "Split every time an boss in the gallery is beaten");
    settings.SetToolTip("split_on_boss_gallery", "Is probably useful if you're running an a bossgallery% category?");
    settings.Add("split_every_ten_percent", false, "Split every 10% of items found");
    settings.SetToolTip("split_every_ten_percent", "Probably isn't useful");
    settings.Add("split_at_100_percent", true, "Split at 100% items");
    settings.SetToolTip("split_at_100_percent", "Probably useful for a 100% category???");
    settings.Add("split_on_sacred_temple", false, "Split when entering the sacred temple");
    settings.SetToolTip("split_on_sacred_temple", "Doesn't include entries from the exit map [19]");
    settings.Add("split_on_every_map_change", false, "Split whenever the map ID changes");
    settings.SetToolTip("split_on_sacred_temple", "Not really recommended to turn this on.");
    settings.Add("split_on_every_new_map_change", false, "Split whenever the map ID changes to a new map");
    settings.SetToolTip("split_on_sacred_temple", "Stores an internal reference to map IDs and splits whenever it encounters a new map");

    settings.Add("remove_cutscene_time", false, "Pause timer in a cutscene");
    settings.SetToolTip("remove_cutscene_time", "Unfortunateley I don't think this value is ever actually used internally in the game for anything so this does nothing.");
    settings.Add("remove_pause_time", false, "Pause timer when the game is paused");
    settings.SetToolTip("remove_pause_time", "Sometimes the game pauses to do cutscenes, but this also handles the pause menu.");
    settings.Add("remove_gameover", true, "Pause Timer in the gameover screen");
    settings.SetToolTip("remove_gameover", "This setting exists so that you're not forced to play on quick gameover mode.");

    settings.Add("start_on_continue", false, "Start the timer when you continue the game");
    settings.SetToolTip("start_on_continue", "I'm not sure if this is actually useful or not but maybe someone wants it.");
    settings.Add("start_on_map_change", false, "Start on map change");
    settings.SetToolTip("start_on_continue", "This is useful for ILs i guess, since most of them start on portal entry. Doesn't count the title screen.");

    settings.Add("reset_on_titlescreen", false, "Reset timer when you go back to the title screen");
    settings.SetToolTip("reset_on_titlescreen", "I'm not sure if this is actually useful or not but maybe someone wants it.");

    settings.Add("cheat_double_speed", false, "Enable when using the double speed cheat");
    settings.SetToolTip("cheat_double_speed", "There might be a way to detect this through pointers but it's annoying");
}

//shutdown {}

init {
    if (settings["cheat_double_speed"]){
        refreshRate = 60;
    } else {
        refreshRate = 30;
    }

    vars.chapterProgress = new [] { 
        30,     // ch1
        160,    // gate 1 mario
        530,    // ch3
        630,    // gate 2 zelda
        930,    // ch5
        1160,   // gate 3 castlevania
        1540,   // ch7
        1770,   // gate 4 megaman
        2010,   // ch9
        2170,   // the vault
        2570,   // ch11
        2600,   // ch12
        2680,   // gate 5 metroid
        3090,   // ch14
        3400,   // gate 6 rpg
        3880,   // ch16
        3940,   // ch17
        4210,   // ch18
        4530,   // ch19
        5050,   // ch20
        5510    // ch21
    };

    vars.seenMapIds = new List<int>();
    
    vars.MapIdTitleScreen = 1;
    vars.MapIdIntroScene = 43;
    vars.MapIdSacredTemple = 236; //the sacred temple's map ID
    vars.MapIdSacredTempleExitMap = 245; //the map you enter when you normally leave the sacred temple
}

exit {
    vars.seenMapIds.Clear();
}
update {
    // check if someone has reset the timer, in which case clear our array
    var time = timer.CurrentTime.RealTime;
    if(time == TimeSpan.Zero && vars.seenMapIds.Count > 0){
        vars.seenMapIds.Clear();
    }
    //if(time > TimeSpan.Zero && vars.seenMapIds.Count == 0){
        //vars.seenMapIds.Add((int)current.mapID);
    //}
}

start {
    // map 1 is the title screen, and map 43 is the intro cutscene on that cliff
    // it's the first thing that appears after you press new game and can only be reached from that
    // however let's safeguard with gameProgress of 0 as well in case weird stuff happens
    if (old.mapID == vars.MapIdTitleScreen && current.mapID == vars.MapIdIntroScene && current.gameProgress == 0){
        vars.seenMapIds.Add((int)current.mapID);
        return true;
    }

    // this setting could be useful for categories like "bossgallery%" i guess
    if (settings["start_on_continue"] && old.mapID == vars.MapIdTitleScreen && current.mapID != vars.MapIdTitleScreen){
        vars.seenMapIds.Add((int)current.mapID);
        return true;
    }
    
    // useful for ILs probably
    // ignores the title screen
    if (settings["start_on_map_change"] && old.mapID != vars.MapIdTitleScreen && old.mapID != current.mapID && current.mapID != vars.MapIdTitleScreen){
        vars.seenMapIds.Add((int)current.mapID);
        return true;
    }
}

reset {
    // reset when we reach the title screen
    if (settings["reset_on_titlescreen"] && old.mapID != vars.MapIdTitleScreen && current.mapID == vars.MapIdTitleScreen){
        return true;
    }

    // reset when we click on the new game button on the main menu
    if (old.mapID == vars.MapIdTitleScreen && current.mapID == vars.MapIdIntroScene && current.gameProgress == 0){
        return true;
    }
}

split {
    if(settings["split_on_every_new_map_change"] && !vars.seenMapIds.Contains((int)current.mapID)){
        vars.seenMapIds.Add((int)current.mapID);

        if((int)old.mapID != vars.MapIdTitleScreen // not coming from title
            && (int)current.mapID != vars.MapIdTitleScreen //not going to title
            && (int)old.mapID != current.mapID
        ){
            return true;
        }
        // otherwise do nothing
    }

    // The "game completed" value changes right before heading to the title in the epilogue
    if (settings["split_on_game_completed"] && current.gameCompleted == 1 && old.gameCompleted == 0){
        return true;
    }

    // Check if the current boss name changed from "having something" to "not having something"
    if (settings["split_on_every_boss"] && !String.IsNullOrEmpty(old.currentBoss) && String.IsNullOrEmpty(current.currentBoss)) {
        // If we have the "split_on_boss_gallery" setting checked we always want to split on a boss death
        // otherwise only split if we're not currently in the boss gallery
        if (settings["split_on_boss_gallery"] || current.bBossGallery == 0) {
            return true;
        }
    }
    
    // Check if the current boss name changed from "not having something" to "having something"
    if (settings["split_on_every_boss_start"] && !String.IsNullOrEmpty(current.currentBoss) && String.IsNullOrEmpty(old.currentBoss)) {
        // If we have the "split_on_boss_gallery" setting checked we always want to split on a boss start
        // otherwise only split if we're not currently in the boss gallery
        if (settings["split_on_boss_gallery"] || current.bBossGallery == 0) {
            return true;
        }
    }

    // Check if our achievement strings were modified and split if one of them were
    if (settings["split_on_achievement"] && (old.tokenRecognitions != current.tokenRecognitions || old.tokenRecognitionsTwo != current.tokenRecognitionsTwo)){
        return true;
    }

    // just check if our old programchips value is different
    if (settings["split_on_every_program_chip"] && old.programChips != current.programChips){
        return true;
    }

    //check in an array if we're at a chapter boundary
    if (settings["split_on_every_chapter"] && old.gameProgress != current.gameProgress && Array.IndexOf(vars.chapterProgress, (int)current.gameProgress) != -1){
        return true;
    }

    // Check a wraparound using modulo with a wide margin.
    if (settings["split_every_ten_percent"] && old.gamePercent % 10 >= 9 && current.gamePercent % 10 <= 1){
        return true;
    }
    // Casted to int to floor the value and hopefully ensure we only trigger once we get the 100
    // and possibly avoid weird impreciseness problems with double?
    if (settings["split_at_100_percent"] && (int)old.gamePercent < 100 && (int)current.gamePercent >= 100){
        return true;
    }
    
    //Check for sacred temple entries
    if(settings["split_on_sacred_temple"] && (int)old.mapID != vars.MapIdSacredTempleExitMap && (int)old.mapID != vars.MapIdSacredTemple && (int)current.mapID == vars.MapIdSacredTemple){
        return true;
    }
    
    if(settings["split_on_every_map_change"] && (int)old.mapID != (int)current.mapID){
        return true;
    }
}

isLoading {
    if (settings["remove_pause_time"] && current.isPaused == 1) {
        return true;
    }
    if (settings["remove_cutscene_time"] && current.inCutscene == 1){
        return true;
    }
    if (settings["remove_gameover"] && current.inGameOver == 1){
        return true;
    }
    return false;
}

// Unfortunately playTime{X} variables count up during the title screen so it's not very useful in actuality
// could use the leveltime and manually track state i guess
// or could store the playTime at the time of clicking Start, store it in the global vars, then subtract it manually?
//gameTime {
//    return new TimeSpan(0, (int)current.playTimeHour, (int)current.playTimeMinute, (int)current.playTimeSecond, (int)(current.playTimeFrameCount/30*100));
//}
