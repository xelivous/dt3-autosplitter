# DT3 Autosplitter

This is a [Livesplit](https://livesplit.org/) autosplitter for [Distorted Travesty 3](http://distortedtravesty.blogspot.com/p/spoiler-warning-this-page-contains.html).

## Address Backup
There's a file in this repo called `address_backup.md` that contains a bunch of extra pointer addresses that aren't currently used in the autosplitter but might be useful at some other point in the future.