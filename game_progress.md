# game progress information
Some key moments in regards to game progress and what they map to. The main motivation is seeing where the bosses are and some key items i guess. 

Format:  
`Value at event start -> value at event end = what the event is`

In some cases the overall game progress isn't used and the sub gate progress is used instead. Usually for optional bosses or simply bosses you can do in any order that isn't directly tied to the overall game state.

```python
# chapter 1 - beginning
20->30 = chapter 1 start
60->70 = grabbed jump boots
100->110 = can now swap characters

# chapter 2 - gate 1 - mario
150->160 = chapter 2 start 
230->240 = (boss) bowser fight start
500->510 = (boss) cackletta start

# chapter 3 - return to overworld
525->530 = chapter 3 start

# chapter 4 - gate 2 - zelda
620->630 = chapter 4 start 
???->??? = (boss) helmet head check
    if gameProgCheck="1" and room="rLink2_PalaceA_11"
    sets gameProgCheck to "3" when fully finished
    index 1 of gameGate2Prog gets set to "3"
???->??? = (boss) dead hand
    if gameProgCheck="3" and room="rLink2_PalaceF_12"
    sets gameProgCheck to "5" when fully finished
    index 2 of gameGate2Prog gets set to "5"
790->795 = (boss) barba
880->890 = (boss) aqua serpent
900->910 = Triforce/Zelda gives thanks & a heart container

# chapter 5 - return to overworld
920->930 = chapter 5 start
980->990 = (boss) Control Virus

# chapter 6 - gate 3 - castlevania
1150->1160 = chapter 6 start 
1180->1190 = (boss) vampire bat
1280->1290 = (boss) dracula
1430->1440 = (boss) Menace
1510->1520 = (boss) Death

# chapter 7 - return to overworld
1530->1540 = chapter 7 start
1680->1690 = (boss) enmity

# chapter 8 - gate 4 - megaman
1760->1770 = chapter 8A start
1830->1840 = (boss) Maoh the Giant
1845->1850 = all mavericks defeated
1850->1860 = chapter 8B start
1890->1900 = (boss) Bospider
1910->1920 = (boss) Bit
1930->1940 = (boss) Byte
1940->1950 = (boss) Bit & Byte
1980->1990 = (boss) Sigma

# chapter 9 - return to overworld
2000->2010 = chapter 9 start
2080->2090 = (boss) army eye

# chapter 10 - The Vault
2160->2170 = chapter 10 start
2210->2220 = grabbed megaphone
2420->2430 = (boss) hex
2530->2540 = end of vault

# chapter 11 - return to overworld
2560->2570 = chapter 11 start 

# chapter 12 - barrens
2590->2600 = chapter 12 start
2620->2630 = (boss) vault demon

# chapter 13 - gate 5 - metroid
2670->2680 = chapter 13 start

2960->2970 = (boss) Mother Brain
3000->3030 = (boss) Ridley
3040->3070 = (boss?) escape sequence

# chapter 14 - return to overworld
3080->3090 = chapter 14 start
3260->3270 = (boss) Malevolence

# chapter 15 - gate 6 - rpg
3390->3400 = chapter 15 start
3400->3410 = chapter 15 act 1 start
3440->3450 = chapter 15 act 2 start
3480->3490 = chapter 15 act 3 start
3490->3500 = chapter 15 act 4 start
3540->3550 = chapter 15 act 12 start
3570->3580 = chapter 15 act 37 start
3590->3600 = chapter 15 act 77 start
3640->3650 = chapter 15 act 293 start
3780->3790 = chapter 15 act 313 start
3800->3810 = chapter 15 act 1000 start

# chapter 16
3870->3880 = chapter 16 start

# chapter 17
3930->3940 = chapter 17 start

# chapter 18
4200-> 4210 = chapter 18 start

# chapter 19
4520->4530 = chapter 19 start

# chapter 20
5040->5050 = chapter 20 start

# chapter 21
5500->5510 = chapter 21 start

```
